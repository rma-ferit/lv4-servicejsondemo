package hr.ferit.brunozoric.servicejsondemo

import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_search_result.view.*
import java.lang.Exception

class SearchResultAdapter : RecyclerView.Adapter<SearchResultHolder>() {

    val results: MutableList<SearchResult> = mutableListOf()

    fun refreshData(newResults: List<SearchResult>) {
        results.clear()
        results.addAll(newResults)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = results.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_search_result, parent, false)
        return SearchResultHolder(view)
    }

    override fun onBindViewHolder(viewHolder: SearchResultHolder, position: Int) {
        viewHolder.bind(results.get(position))
    }
}

class SearchResultHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(result: SearchResult) {
        itemView.name.text = result.show.name
        itemView.summary.text = result.show.summary

        val url = result.show.thumbnailUrl?.smallThumbnailUrl ?: null

        Picasso.get()
            .load(url)
            .placeholder(R.mipmap.ic_launcher)
            .error(android.R.drawable.stat_notify_error)
            .into(itemView.thumbnail)
    }
}
