package hr.ferit.brunozoric.servicejsondemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.LinearLayout
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class MainActivity : AppCompatActivity(), Callback<List<SearchResult>> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpUi()
    }

    private fun setUpUi() {
        searchAction.setOnClickListener{findShows()}
        showsFound.adapter = SearchResultAdapter()
        showsFound.layoutManager = LinearLayoutManager(
            this,
            RecyclerView.VERTICAL,
            false
        )
        showsFound.addItemDecoration(
            DividerItemDecoration(this, RecyclerView.VERTICAL)
        )
        showsFound.itemAnimator = DefaultItemAnimator()
    }

    private fun findShows() {
        val name = showNameInput.text.toString()
        Networking.showSearchService.findShow(name).enqueue(this)
    }

    override fun onFailure(call: Call<List<SearchResult>>, t: Throwable) {
        Toast.makeText(this, R.string.networkError, Toast.LENGTH_SHORT).show()
    }

    override fun onResponse(call: Call<List<SearchResult>>, response: Response<List<SearchResult>>) {
        val results = response.body() ?: listOf()
        Log.d("Results", results.toString())
        (showsFound.adapter as SearchResultAdapter).refreshData(results)
    }
}
