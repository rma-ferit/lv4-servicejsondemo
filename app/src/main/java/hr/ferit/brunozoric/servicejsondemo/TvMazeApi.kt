package hr.ferit.brunozoric.servicejsondemo

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

const val BASE_URL = "https://api.tvmaze.com/"

interface TvMazeApi {

    @GET("search/shows")
    fun findShow(@Query("q") name: String): Call<List<SearchResult>>
}