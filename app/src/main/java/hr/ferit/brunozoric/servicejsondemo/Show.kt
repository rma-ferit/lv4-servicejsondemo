package hr.ferit.brunozoric.servicejsondemo

import com.google.gson.annotations.SerializedName

data class SearchResult (
    @SerializedName("score") val score: Double,
    @SerializedName("show") val show: Show
)

data class Show (
    @SerializedName("id") val id: Long,
    @SerializedName("name") val name: String,
    @SerializedName("summary") val summary: String,
    @SerializedName("image") val thumbnailUrl: Thumbnail
)

data class Thumbnail(
    @SerializedName("medium") val smallThumbnailUrl: String,
    @SerializedName("original") val largeThumbnailUrl: String
)

