package hr.ferit.brunozoric.servicejsondemo

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Networking{
    val showSearchService: TvMazeApi = Retrofit.Builder()
        .addConverterFactory(ConverterFactory.converterFactory)
        .client(HttpClient.client)
        .baseUrl(BASE_URL)
        .build()
        .create(TvMazeApi::class.java)
}

object ConverterFactory{
    val converterFactory = GsonConverterFactory.create()
}

object HttpClient{
    val client = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .build()
}

